#  1. Create an abstract class called Animal that has the following abstract methods  
# Abstract Methods: eat(food), make_sound()

from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass





# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# Properties: name, breed, age
# Methods: getters and setters, implementation of abstract methods, call()

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # getters
    def get_name(self):
        print(f"The name of the cat is {self._name}")

    def get_breed(self):
        print(f"The breed of the cat is {self._breed}")

    def get_age(self):
        print(f"The age of the cat is {self._age}")

    # setters
    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaaa!")

    def call(self):
        print(f"{self._name}, come on!")

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # getters
    def get_name(self):
        print(f"The name of the dog is {self._name}")

    def get_breed(self):
        print(f"The breed of the dog is {self._breed}")

    def get_age(self):
        print(f"The age of the dog is {self._age}")

    # setters
    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self._name}!")

Isis = Dog("Isis", "Retriever", 4)
Isis.eat("Steak")
Isis.make_sound()
Isis.call()

Puss = Cat("Puss", "Persian", 7)
Puss.eat("Tuna")
Puss.make_sound
Puss.call()